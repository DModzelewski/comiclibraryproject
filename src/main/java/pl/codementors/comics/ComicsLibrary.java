package pl.codementors.comics;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents comics library. Contains inner collection with comics, exposes methods for adding and removing comics.
 * Allows for changing covers of all contained comics.
 *
 * @author psysiu
 */
public class ComicsLibrary {

    // for logs
    public static final Logger log = Logger.getLogger(ComicsLibrary.class.getSimpleName());

    /**
     * Set of comics contained in the library.
     */
    private Set<Comic> comics = new HashSet<>();

    /**
     *
     * @return All comics in the library. The returned collection is unmodifiable so it can not be changed
     * outside the library.
     */
    public final Collection<Comic> getComics() {
        return comics;
    }

    /**
     * Adds comic to the library. If comic is already in the library does nothing. If comic is null does nothing.
     *
     * @param comic Comic to be added to the library.
     */
    public void add(Comic comic) {
        if (comic!=null){
            if (!comics.contains(comic)){
                comics.add(comic);
            }
        }

    }

    /**
     * Removes comic from the library. If comics is not present in the library does nothing.
     *
     * @param comic Comic to be removed from the library.
     */
    public void remove(Comic comic) {
        if (comics.contains(comic)){
            comics.remove(comic);
        }
    }

    /**
     * Changes covers of all comics in the library.
     *
     * @param cover Cover type for all comics in the library.
     */
    public void changeCovers(Comic.Cover cover) {
        for (Comic c : comics){
            c.setCover(cover);
        }
    }

    /**
     *
     * @return All authors of all comics in the library. Each author is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public final Collection<String> getAuthors() {
        Collection <String> authors = new ArrayList<>();
        for (Comic c : comics){
            if (!authors.contains(c.getAuthor())){
                authors.add(c.getAuthor());
            }
        }
        return authors;
    }

    /**
     *
     * @return All series of all comics in the library. Each series is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public final Collection<String> getSeries() {
        Collection <String> series = new ArrayList<>();
        for (Comic c : comics){
            if (!series.contains(c.getSeries())){
                series.add(c.getSeries());
            }
        }
        return series;
    }

    /**
     * Loads comics from file. Method uses FileReader and Scanner for reading comics from file.
     *
     * The file structure is:
     * number_of_comics (one line with one number, nextInt())
     * comics title (one line with spaces, nextLine())
     * comics author (one line with spaces, nextLine())
     * comics series (one line with spaces, nextLine())
     * cover (one line with one word, next())
     * publish_month (one line with one number, nextInt())
     * publish_year (one line with one number, nextInt())
     *
     * The proper sequence for reading file is to call nextInt(); skip("\n"); to read number of comics.
     * Then in loop call nextLine(); nextLine(); nextLine(), next(); nextInt(); nextInt(); skip("\n").
     *
     * If file does not exists, or is directory, or can not be read, method just ignores it and does nothing.
     *
     * @param file File from which comics will be loaded.
     */
    public void load(File file) {
        try(FileReader fr = new FileReader(file)){
            Scanner input = new Scanner(fr);
            int comicNumber = input.nextInt();
            input.skip("\n");
            for (int i = 0; i < comicNumber; i++){
                String title = input.nextLine();
                String author = input.nextLine();
                String series = input.nextLine();
                Comic.Cover cover = Comic.Cover.valueOf(input.next());
                int month = input.nextInt();
                int year = input.nextInt();
                input.skip("\n");
                comics.add(new Comic(title, author, series, cover, year, month));
            }

        }catch (Exception ex){
            log.log(Level.WARNING, ex.getMessage(), ex);
            //method ignored
        }
    }

    /**
     * Counts all comics with the same provided series name.
     *
     * @param series Name of the series for which comics will be counted.
     * @return Number of comics from the same provided series.
     */
    public int countBySeries(String series) {
        int i = 0; //iterator
        for (Comic c : comics){
            if(c.getSeries().equals(series)){
                i++;
            }
        }
        return i;
    }

    /**
     * Counts all comics with the same provided author.
     *
     * @param author Author for which comics will be counted.
     * @return Number of comics with the same author.
     */
    public int countByAuthor(String author) {
        int i=0; //iterator
        for (Comic c : comics){
            if(c.getAuthor().equals(author)){
                i++;
            }
        }
        return i;
    }

    /**
     * Counts all comics with the same provided publish hear.
     *
     * @param year Publish year for which comics will be counted.
     * @return Number of comics from the same provided publish year.
     */
    public int countByYear(int year) {
        int i=0; //iterator
        for (Comic c: comics){
            if(c.getPublishYear()==(year)){
                i++;
            }
        }
        return i;
    }

    /**
     * Counts all comics with the same provided publish hear and month.
     *
     * @param year Publish year for which comics will be counted.
     * @param month Publish mnt for which comics will be counted.
     * @return Number of comics from the same provided publish year and month.
     */
    public int countByYearAndMonth(int year, int month) {
        int i=0;//iterator
        for (Comic c : comics){
            if(c.getPublishYear()==year&&c.getPublishMonth()==month){
                i++;
            }
        }
        return i;
    }

    /**
     * Removes all comics with publish year smaller than the provided year. For the removal process
     * method uses iterator.
     *
     * @param year Provided yer.
     */
    public void removeAllOlderThan(int year) {
        for (Iterator<Comic> it = comics.iterator(); it.hasNext(); ){
            Comic c = it.next();
            if(c.getPublishYear()<year){
                it.remove();
            }
        }
    }

    /**
     * Removes all comics written by the specified author. For the removal process method uses iterator.
     *
     * @param author Provided author.
     */
    public void removeAllFromAuthor(String author) {
        for (Iterator<Comic> it = comics.iterator(); it.hasNext(); ){
            Comic c = it.next();
            if(c.getAuthor().equals(author)){
                it.remove();
            }
        }
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping author->comics. Map keys are names of the authors (String) present in the library. Map values are
     * collection (e.g.: HashSet<Comic>) of comics for specified author.
     */
    public Map<String, Collection<Comic>> getAuthorsComics() {
        Map<String, Collection<Comic>> authorsComics = new HashMap<>();
        for (Comic c : comics) {
            if (!authorsComics.containsKey(c.getAuthor())) {
                authorsComics.put(c.getAuthor(), new HashSet<>());
                authorsComics.get(c.getAuthor()).add(c);
            } else {
                authorsComics.get(c.getAuthor()).add(c);
            }
        }
        return authorsComics;
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping publish year->comics. Map keys are publish year (Integer, generics can not be simple types
     * so instead int the Integer is used) present in the library. Map values are collection (e.g.: HashSet<Comic>)
     * of comics for specified author.
     */
    public Map<Integer, Collection<Comic>> getYearsComics() {
        Map<Integer, Collection<Comic>> comicsByYear = new HashMap<>();
        for (Comic c : comics){
            if(!comicsByYear.containsKey(c.getPublishYear())){
                comicsByYear.put(c.getPublishYear(), new HashSet<>());
                comicsByYear.get(c.getPublishYear()).add(c);
            } else {
                comicsByYear.get(c.getPublishYear()).add(c);
            }
        }
        return comicsByYear;
    }

    public void removeAllNewerThan(int year){
        for (Iterator<Comic> it = comics.iterator(); it.hasNext(); ){
            Comic c = it.next();
            if(c.getPublishYear()>year){
                it.remove();
            }
        }
    }

    public void removeAllFromYearAndMonth(int year, int month){
        for(Iterator<Comic> it = comics.iterator(); it.hasNext(); ){
            Comic c = it.next();
            if(c.getPublishYear()==year&&c.getPublishMonth()==month){
                it.remove();
            }
        }
    }

    public Map<Pair<Integer,Integer>, Collection<Comic>> getYearsMonthsComics(){
        // create initial vars
        int year;
        int month;
        // create map with pair as key
        Map<Pair<Integer, Integer>, Collection<Comic>> comicsFromYearMonth = new HashMap<>();
        // do things. lots of things

        for (year = 1867; year < 2018; year++){
            for(month = 1; month < 13; month++){
                //create pair
                Pair<Integer,Integer> yearMonthPairs = new ImmutablePair<>(year, month);
                for(Comic c : comics){
                    if(c.getPublishMonth()==month && c.getPublishYear()==year){
                        yearMonthPairs.getLeft().equals(year);
                        yearMonthPairs.getRight().equals(month);
                        if(!comicsFromYearMonth.containsKey(yearMonthPairs)){
                            comicsFromYearMonth.put(yearMonthPairs, new HashSet<>());
                            comicsFromYearMonth.get(yearMonthPairs).add(c);
                        } else {
                            comicsFromYearMonth.get(yearMonthPairs).add(c);
                        }
                    }
                }
            }

        }
        return comicsFromYearMonth;
    }
}
