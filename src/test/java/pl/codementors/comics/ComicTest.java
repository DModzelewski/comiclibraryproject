package pl.codementors.comics;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class ComicTest {

    Comic comic;

    @Rule
    public ExpectedException exceptionGrabber = ExpectedException.none();

    @Before
    public void prepare(){
        comic = new Comic();
    }

    @Test
    public void Comic_noParams_createsInstance() {
        Comic comic = new Comic();
        assertNotNull(comic);
    }

    @Test
    public void Comic_noParams_createsInstanceWith1867PublishYear() {
        Comic comic = new Comic();
        assertEquals(comic.getPublishYear(), 1867);
    }

    @Test
    public void Comic_noParams_createsInstanceWith1PublishMonth() {
        Comic comic = new Comic();
        assertEquals(comic.getPublishMonth(), 1);
    }

    @Test
    public void Comic_noParams_createsInstanceWithEmptyTitle() {
        Comic comic = new Comic();
        assertEquals(comic.getTitle(), "");
    }

    @Test
    public void Comic_noParams_createsInstanceWithEmptyAuthor() {
        Comic comic = new Comic();
        assertEquals(comic.getAuthor(), "");
    }

    @Test
    public void Comic_noParams_createsInstanceWithSoftCover() {
        Comic comic = new Comic();
        assertEquals(comic.getCover(), Comic.Cover.SOFT);
    }

    @Test
    public void Comic_validParams_createsInstanceWithValidPublishYear() {
        Comic comic = new Comic("Batman", "Bob Kane","Batman", Comic.Cover.SOFT, 1939, 2);
        assertEquals(comic.getPublishYear(), 1939);
    }

    @Test
    public void Comic_validParams_createsInstanceWithValidPublishMonth() {
        Comic comic = new Comic("Batman", "Bob Kane", "Batman", Comic.Cover.SOFT, 1939, 2);
        assertEquals(comic.getPublishMonth(), 2);
    }

    @Test
    public void Comic_toBigPublishYear_createsInstanceWithDefaultPublishYear() {
        Comic comic = new Comic("Batman", "Bob Kane", "Batman",Comic.Cover.SOFT, 2018, 2);
        assertEquals(comic.getPublishYear(), 1867);
    }

    @Test
    public void Comic_toSmallPublishYear_createsInstanceWithDefaultPublishYear() {
        Comic comic = new Comic("Batman", "Bob Kane", "Batman",Comic.Cover.SOFT, 1866, 2);
        assertEquals(comic.getPublishYear(), 1867);
    }

    @Test
    public void Comic_toBigPublishMonth_createsInstanceWithDefaultPublishMonty() {
        Comic comic = new Comic("Batman", "Bob Kane", "Batman",Comic.Cover.SOFT, 1939, 13);
        assertEquals(comic.getPublishMonth(), 1);
    }

    @Test
    public void Comic_toSmallPublishMonth_createsInstanceWithDefaultPublishMonth() {
        Comic comic = new Comic("Batman", "Bob Kane", "Batman",Comic.Cover.SOFT, 1939, 0);
        assertEquals(comic.getPublishMonth(), 1);
    }

    @Test
    public void setPublishYear_validPrams_setsPublishYear() {
        Comic comic = new Comic();
        comic.setPublishYear(2013);
        assertEquals(comic.getPublishYear(), 2013);
    }

    @Test
    public void setPublishYear_toSmallParam_doesNothing() {
        Comic comic = new Comic();
        comic.setPublishYear(1866);
        assertNotEquals(comic.getPublishYear(), 1866);
    }

    @Test
    public void setPublishYear_toBigParam_doesNothing() {
        Comic comic = new Comic();
        comic.setPublishYear(2018);
        assertNotEquals(comic.getPublishYear(), 2018);
    }

    @Test
    public void setPublishMonth_validPrams_setsPublishMonth() {
        Comic comic = new Comic();
        comic.setPublishMonth(11);
        assertEquals(comic.getPublishMonth(), 11);
    }

    @Test
    public void setPublishMonth_toSmallParam_doesNothing() {
        Comic comic = new Comic();
        comic.setPublishMonth(0);
        assertNotEquals(comic.getPublishMonth(), 0);
    }

    @Test
    public void setPublishMonth_toBigParam_doesNothing() {
        Comic comic = new Comic();
        comic.setPublishMonth(13);
        assertNotEquals(comic.getPublishMonth(), 13);
    }

    @Test
    public void setSeries_paramProvided_paramSet() {
        Comic comic = new Comic();
        comic.setSeries("Batman");
        assertEquals("Batman", comic.getSeries());
    }

    @Test
    public void setAuthor_paramProvided_paramSet() {
        Comic comic = new Comic();
        comic.setAuthor("Bob Kane");
        assertEquals("Bob Kane", comic.getAuthor());
    }

    @Test
    public void setTitle_paramProvided_paramSet() {
        Comic comic = new Comic();
        comic.setTitle("Batman");
        assertEquals("Batman", comic.getTitle());
    }

    @Test
    public void equals_sameReferences_returnsTrue() {
        Comic comic = new Comic();
        assertEquals(comic, comic);
    }

    @Test
    public void equals_differentReferencesSameValues_returnsTrue() {
        Comic comic1 = new Comic("Batman", "Bob Kane", "Batman",Comic.Cover.SOFT, 1939, 2);
        Comic comic2 = new Comic("Batman", "Bob Kane", "Batman",Comic.Cover.SOFT, 1939, 2);
        assertEquals(comic1, comic2);
    }

    @Test
    public void equals_differentReferencesDifferentValues_returnsFalse() {
        Comic comic1 = new Comic("Batman", "Bob Kane", "Batman",Comic.Cover.SOFT, 1939, 2);
        Comic comic2 = new Comic("Betman", "Beb Kane", "Batman",Comic.Cover.SOFT, 1938, 1);
        assertNotEquals(comic1, comic2);
    }

    //My tests:
    @Test
    public void comic_noParams_createsNullSeries(){
        Comic comic = new Comic();
        assertNull(comic.getSeries());
    }

    @Test
    public void setAuthorToNull_ReturnsNull(){
        Comic comic = new Comic();
        comic.setAuthor(null);
        assertNull(comic.getAuthor());
    }

    @Test
    public void setTitleToNull_ReturnsNull(){
        Comic comic = new Comic();
        comic.setTitle(null);
        assertNull(comic.getTitle());
    }

    @Test
    public void setCoverToNull_ReturnsNull(){
        Comic comic = new Comic();
        comic.setCover(null);
        assertNull(comic.getCover());
    }
}
